using UnityEngine;

namespace Main.Code.Audio {
    public class AudioInstance 
    {
        private AudioManager _audioManager;
        private AudioType _audioType;
        private AudioSource _audioSourceCache;
        
        public AudioInstance(AudioSource audioSource, AudioType audioType)
        {
            _audioSourceCache = audioSource;
            _audioType = audioType;
        }

        public void Initialize(AudioManager audioManager)
        {
            _audioManager = audioManager;
            _audioManager.OnAudioChange[(int)_audioType] += Enable;
            Enable(_audioManager.IsAudioActive(_audioType));
        }

        public void Play()
        {
            _audioSourceCache.Play();
        }

        private void Enable(bool isEnable)
        {
            _audioSourceCache.enabled = isEnable;
        }
    }
    
}

