﻿namespace Main.Code.Audio
{
    public enum AudioType
    {
        Music,
        Sound,
        Vibrations
    }
}