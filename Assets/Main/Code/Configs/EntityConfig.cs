using UnityEngine;
using UnityEngine.Serialization;

namespace Main.Code.Gameplay.Enemy
{
    [CreateAssetMenu(fileName = "EntityConfig", menuName = "Configs/EntityConfig")]
    public class EntityConfig : ScriptableObject
    {
        [SerializeField] private int _health;
        public int Health => _health;
    }
}