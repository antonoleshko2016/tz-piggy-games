using UnityEngine;

namespace Main.Code.Gameplay.Enemy
{
    [CreateAssetMenu(fileName = "ArrowConfig", menuName = "Configs/ArrowConfig")]
    public class ArrowConfig : ScriptableObject
    {
        [SerializeField] private int _damage;
        [SerializeField] private float _speed;
        public int Damage => _damage;
        public float Speed => _speed;
    }
}