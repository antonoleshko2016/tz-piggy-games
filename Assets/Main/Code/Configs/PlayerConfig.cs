using Main.Code.Gameplay.Arrow;
using UnityEngine;
using UnityEngine.Serialization;

namespace Main.Code.Gameplay.Enemy
{
    [CreateAssetMenu(fileName = "PlayerConfig", menuName = "Configs/PlayerConfig")]
    public class PlayerConfig : EntityConfig
    {
        [SerializeField] private float shootRecharge = 3;
        [SerializeField] private ArrowView _arrowView;
        [SerializeField] private float _detectEnemyRadius;
        public float ShootRecharge => shootRecharge;
        public ArrowView ArrowView => _arrowView;
        public float DetectEnemyRadius => _detectEnemyRadius;
    }
}