using UnityEngine;
using UnityEngine.Serialization;

namespace Main.Code.Gameplay.Enemy
{
    [CreateAssetMenu(fileName = "EnemyConfig", menuName = "Configs/EnemyConfig")]
    public class EnemyConfig : EntityConfig
    {
        [SerializeField] private float _speed;
        public float Speed => _speed;
    }
}