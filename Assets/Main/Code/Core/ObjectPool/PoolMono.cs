﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace AwesomeTools.Optimization
{
    public class PoolMono<T> where T : MonoBehaviour
    {
        private readonly Transform _container;
        private readonly T _prefab; 
        private readonly List<T> _freeElements = new(); 
        private readonly bool _autoExpand; 
        private List<T> _pool; 
        private readonly bool _reusable;
        private DiContainer _diContainer;

        public PoolMono(T prefab, DiContainer diContainer, int countInPool, Transform container = null, bool autoExpand = true, bool reusable = true)
        {
            _diContainer = diContainer;
            _reusable = reusable;
            _prefab = prefab;
            _container = container;
            _autoExpand = autoExpand;
            CreatePool(countInPool);
        }
        
        private bool HasFreeElement(out T element)
        {
            foreach (var mono in _pool.Where(mono => !mono.gameObject.activeInHierarchy))
            {
                _freeElements.Add(mono); 
            }

            if (_freeElements.Count == 0)
            {
                element = null;
                return false;
            }

            element = _freeElements[Random.Range(0, _freeElements.Count)];
            element.gameObject.SetActive(true); 
            _freeElements.Clear();
            return true;
        }
        
        public bool HasFreeElement()
        {
            foreach (var mono in _pool.Where(mono => !mono.gameObject.activeInHierarchy))
            {
                _freeElements.Add(mono);
            }

            if (_freeElements.Count == 0)
                return false; 

            _freeElements.Clear();
            return true;
        }
        
        public T GetFreeElement()
        {
            if (HasFreeElement(out var element))
            {
                if (!_reusable)
                {
                    _pool.Remove(element);
                }

                return element;
            }
            if (_autoExpand)
            {
                return CreateObject(true);
            }

            throw new System.Exception($"There are no elements in the pool of type {typeof(T)}");
        }

        public void ReturnToPool(T element)
        {
            if (!_pool.Contains(element))
            {
                Debug.LogError("Trying to return an object to the pool that doesn't belong to this pool.");
                return;
            }

            element.gameObject.SetActive(false);
        }
        
        private void CreatePool(int countInPool)
        {
            _pool = new List<T>();

            for (int i = 0; i < countInPool; i++)
            {
                CreateObject();
            }
        }
        
        private T CreateObject(bool isActiveByDefault = false)
        {
            var createdObj = _diContainer.InstantiatePrefab(_prefab,Vector3.up * 100, Quaternion.identity , _container).GetComponent<T>();
            createdObj.gameObject.SetActive(isActiveByDefault);
            _pool.Add(createdObj);
            return createdObj;
        }
    }
}
