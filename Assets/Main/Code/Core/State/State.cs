﻿using System;
using Main.Code.Core.Additional;
using Zenject;

namespace Main.Code.Core.State
{
    public abstract class State : IDisposable
    {
        [Inject] protected Timer _timer;
        public void Initialize()
        {
            _timer.OnTick += Update;
            _timer.FixedTick += FixedUpdate;
            Initialized();
        }

        protected abstract void Initialized();
        public void Dispose()
        {
            _timer.OnTick -= Update;
            _timer.FixedTick -= FixedUpdate;
            OnDispose();
        }
        
        protected abstract void OnDispose();

        protected virtual void Update()
        {
            
        }

        protected virtual void FixedUpdate()
        {
            
        }
    }
}