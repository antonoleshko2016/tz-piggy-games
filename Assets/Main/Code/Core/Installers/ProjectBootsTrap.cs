using Main.Code.Audio;
using Main.Code.Configs;
using Main.Code.Core.Additional;
using UnityEngine;
using Zenject;

namespace Main.Code.Core
{
    public class ProjectBootsTrap : MonoInstaller
    {
        [SerializeField] private Timer _timer;
        [SerializeField] private GameConfig _gameConfig;
        public override void InstallBindings()
        {
            Container.Bind<GameConfig>().FromInstance(_gameConfig).AsSingle();
            Container.Bind<Timer>().FromInstance(_timer).AsSingle();
            Container.Bind<Bank>().AsSingle().NonLazy();
            Container.Bind<AudioManager>().AsSingle().NonLazy();
        }
    }
}