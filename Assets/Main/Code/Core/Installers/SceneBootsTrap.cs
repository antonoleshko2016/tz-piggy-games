﻿using Main.Code.Core.UI;
using Main.Code.Gameplay.Game;
using UnityEngine;
using Zenject;

namespace Main.Code.Core
{
    public class SceneBootsTrap : MonoInstaller
    {
        [SerializeField] private GameView _gameView;
        public override void InstallBindings()
        {
            Container.Bind<GameView>().FromInstance(_gameView);
            Container.Bind<HudManager>().AsSingle().NonLazy();
            Container.Bind<GameController>().AsSingle().NonLazy();
        }
    }
}