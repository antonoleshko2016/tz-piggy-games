using AwesomeTools.Optimization;
using Main.Code.Gameplay.Enemy;
using Main.Code.Gameplay.Game;
using UnityEngine;
using Zenject;

namespace Main.Code.Gameplay.Spawners
{
    public class EnemySpawner
    {
        private GameView _gameView;
        private PoolMono<EnemyView> _enemyPool;
        private readonly int _startCountInPool;
        
        public EnemySpawner(DiContainer diContainer,  GameView gameView)
        {
            _gameView = gameView; 
            _enemyPool =
                new PoolMono<EnemyView>(_gameView.EnemyPrefab, diContainer, _startCountInPool,
                    _gameView.EnemySpawnPosition.parent); //Should be other valid parent);
        }
        
        public EnemyView SpawnEnemy()
        {
            EnemyView enemy = _enemyPool.GetFreeElement();
            enemy.transform.position = _gameView.EnemySpawnPosition.position;
            enemy.Activate();
            return enemy;
        }

        public void ReturnToPool(EnemyView enemy)
        {
            _enemyPool.ReturnToPool(enemy);
        }
    }
}