namespace Main.Code.Gameplay.Interfaces
{
    public interface IDamageable
    {
        public void Hit(int damage);
    }
}