using System;
using DG.Tweening;
using Main.Code.Gameplay.Enemy;
using Main.Code.Gameplay.Enviroment;
using Main.Code.Gameplay.Interfaces;
using UnityEngine;

namespace Main.Code.Gameplay.Arrow
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class ArrowView : MonoBehaviour
    {
        [SerializeField] private ArrowConfig _arrowConfig;
        [SerializeField] private SpriteRenderer _spriteRenderer;
        private Rigidbody2D _rigidbody;
        private bool _isOnGround;
        private Transform _transform;
        private Vector2 _forceDirection;
        private float _acceleration;

        private void Awake()
        {
            _transform = transform;
            _rigidbody = GetComponent<Rigidbody2D>();
            _rigidbody.bodyType = RigidbodyType2D.Kinematic;
        }

        public void Shoot(Vector2 forceDirection, float acceleration)
        {
            _acceleration = acceleration;
            _rigidbody.bodyType = RigidbodyType2D.Dynamic;
            _forceDirection = forceDirection;
            _rigidbody.AddForce(_forceDirection, ForceMode2D.Impulse);
        }

        private void Update()
        {
            _rigidbody.AddForce(_forceDirection.normalized * (_acceleration * Time.deltaTime), ForceMode2D.Impulse);
            float angle = Mathf.Atan2(_rigidbody.velocity.y, _rigidbody.velocity.x) * Mathf.Rad2Deg;
            _transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (_isOnGround) return;
            IDamageable damageable = other.GetComponent<IDamageable>();
            if (damageable != null)
            {
                Destroy(gameObject);
                damageable.Hit(_arrowConfig.Damage);
            }

            Ground ground = other.GetComponent<Ground>();
            if (ground != null)
            {
                _rigidbody.bodyType = RigidbodyType2D.Static;
                DestroyRoutine();
            }
        }

        private void DestroyRoutine()
        {
            _isOnGround = true;
            _spriteRenderer.DOFade(0, 3).OnComplete((() => {Destroy(gameObject);}));
        }
    }
}