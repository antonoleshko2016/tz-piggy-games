using Main.Code.Core.State;

namespace Main.Code.Gameplay.Enemy.EnemyStates
{
    public abstract class EnemyState : State
    {
        protected EnemyView _enemyView;

        public EnemyState(EnemyView enemyView)
        {
            _enemyView = enemyView;
        }
    }
}