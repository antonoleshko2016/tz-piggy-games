namespace Main.Code.Gameplay.Enemy.EnemyStates
{
    public class InitializeState : EnemyState
    {
        public InitializeState(EnemyView enemyView) : base(enemyView)
        {
        }
        protected override void Initialized()
        {
            _enemyView.ChangeState<MovingState>();
        }

        protected override void OnDispose()
        {
            
        }

       
    }
}