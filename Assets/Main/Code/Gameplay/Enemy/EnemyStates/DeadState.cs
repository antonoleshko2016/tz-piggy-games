using Main.Code.Gameplay.Game;
using Zenject;

namespace Main.Code.Gameplay.Enemy.EnemyStates
{
    public class DeadState : EnemyState
    {
        [Inject] private GameController _gameController;
        protected override void Initialized()
        {
            _gameController.enemySpawner.ReturnToPool(_enemyView);
        }

        protected override void OnDispose()
        {
            
        }

        public DeadState(EnemyView enemyView) : base(enemyView)
        {
        }
    }
}