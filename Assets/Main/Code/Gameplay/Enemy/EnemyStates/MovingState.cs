using UnityEngine;

namespace Main.Code.Gameplay.Enemy.EnemyStates
{
    public class MovingState : EnemyState
    {
        private Transform _transform;
        private Rigidbody2D _rigidbody;
        private float _moveSpeed;

        public MovingState(EnemyView enemyView, Rigidbody2D rigidbody, float moveSpeed) : base(enemyView)
        {
            _moveSpeed = moveSpeed;
            _rigidbody = rigidbody;
            _transform = enemyView.transform;
        }
        protected override void Initialized()
        {
            
        }

        protected override void OnDispose()
        {
            
        }
        protected override void FixedUpdate()
        {
            _transform.position += Vector3.left * (_moveSpeed * Time.fixedDeltaTime);
            if (_transform.position.x < -15)
            {
                _enemyView.ChangeState<DeadState>();
            }
        }
    }
}