using System;
using Main.Code.Gameplay.Interfaces;
using UnityEngine;

namespace Main.Code.Gameplay.Enemy
{
    public class EntityView : MonoBehaviour, IDamageable
    {
        public event Action OnHit;
        public event Action OnDeath;
        private float _health;
        private bool _isAlive;
        public bool IsAlive => _isAlive;

        protected void SetHealth(int health)
        {
            _health = health;
            _isAlive = true;
        }
        
        public void Hit(int damage)
        {
            if(!_isAlive) return;
            Debug.Log(damage + " Damage deal");
            _health -= damage;
            OnHit?.Invoke();
            if (_health <= 0)
            {
                _health = 0;
                _isAlive = false;
                OnDeath?.Invoke();
            }
        }
    }
}