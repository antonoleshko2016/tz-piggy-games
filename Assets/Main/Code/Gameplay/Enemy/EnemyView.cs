using System;
using Main.Code.Core.State;
using Main.Code.Gameplay.Enemy.EnemyStates;
using UnityEngine;
using Zenject;

namespace Main.Code.Gameplay.Enemy
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class EnemyView : EntityView
    {
        [SerializeField] private EnemyConfig _enemyConfig;
        private StateController _stateController;
        private Rigidbody2D _rigidbody2D;

        [Inject]
        private void Construct(DiContainer diContainer)
        {
            _rigidbody2D = GetComponent<Rigidbody2D>();
            _stateController = new StateController();
            diContainer.Inject(_stateController);
            _stateController.AddState(new InitializeState(this));
            _stateController.AddState(new MovingState(this, _rigidbody2D, _enemyConfig.Speed));
            _stateController.AddState(new DeadState(this));
        }

        public void Activate()
        {
            SetHealth(_enemyConfig.Health);
            OnDeath += Death;
            ChangeState<InitializeState>();
        }

        private void Death()
        {
            ChangeState<DeadState>();
        }

        public void ChangeState<T>() where T : EnemyState
        {
            _stateController.ChangeState<T>();
        }
    }
}