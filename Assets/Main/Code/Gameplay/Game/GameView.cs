﻿using System.Collections.Generic;
using Main.Code.Core.UI;
using Main.Code.Gameplay.Enemy;
using Main.Code.Gameplay.Player;
using UnityEngine;
using UnityEngine.Serialization;

namespace Main.Code.Gameplay.Game
{
    public sealed class GameView : MonoBehaviour
    {
        [SerializeField] private AudioSource _backgroundMusic;
        [SerializeField] private PlayerView _player;
        [SerializeField] private BaseHud[] _huds;
        [SerializeField] private EnemyView _enemyPrefab;
        [SerializeField] private Transform _enemySpawnPosition;
        public AudioSource BackgroundMusic => _backgroundMusic;
        public PlayerView Player => _player;
        public EnemyView EnemyPrefab => _enemyPrefab;
        public Transform EnemySpawnPosition => _enemySpawnPosition;

        public IEnumerable<IHud> AllHuds()
        {
            foreach (var hud in _huds)
            {
                yield return hud;
            }
        }
    }
}