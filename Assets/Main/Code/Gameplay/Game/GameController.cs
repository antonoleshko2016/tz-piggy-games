﻿using System;
using Main.Code.Audio;
using Main.Code.Configs;
using Main.Code.Core.State;
using Main.Code.Gameplay.Game.States;
using Main.Code.Gameplay.Spawners;
using Zenject;

namespace Main.Code.Gameplay.Game
{
    public sealed class GameController : IDisposable
    {
        [Inject] private GameConfig _gameConfig;
        [Inject] private GameView _gameView;
        [Inject] private AudioManager _audioManager;
        private StateController _stateController;
        private GameModel _gameModel;
        
        private AudioInstance _backgroundMusic;
        public EnemySpawner enemySpawner { get; set; }

        [Inject]
        public void Construct(DiContainer diContainer)
        {
            _stateController = new StateController();
            diContainer.Inject(_stateController);

            _stateController.AddState(new GameInitializeState(_backgroundMusic));
            _stateController.AddState(new GameStartedState());
            ChangeState<GameInitializeState>();
        }

        public void ChangeState<T>() where T : GameState
        {
            _stateController.ChangeState<T>();
        }

        public void Dispose()
        {
            _audioManager.RemoveAudioInstance(_backgroundMusic);
        }

        public void SpawnEnemy()
        {
            enemySpawner.SpawnEnemy();
        }
    }
}