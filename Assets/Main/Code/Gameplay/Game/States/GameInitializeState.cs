using Main.Code.Audio;
using Main.Code.Core.UI;
using Main.Code.Gameplay.Spawners;
using Main.Code.UI;
using UnityEngine;
using Zenject;
using AudioType = Main.Code.Audio.AudioType;

namespace Main.Code.Gameplay.Game.States
{
    public class GameInitializeState : GameState
    {
        [Inject] private HudManager _hudManager;
        [Inject] private GameController _gameController;
        [Inject] private GameView _gameView;
        [Inject] private AudioManager _audioManager;
        [Inject] private DiContainer _diContainer;
        private AudioInstance _backgroundMusic;
        private EnemySpawner _enemySpawner;

        public GameInitializeState(AudioInstance backgroundMusic)
        {
            _backgroundMusic = backgroundMusic;
        }
        protected override void Initialized()
        {
            _gameController.enemySpawner = new EnemySpawner(_diContainer, _gameView);
            
            _backgroundMusic = new AudioInstance(_gameView.BackgroundMusic, AudioType.Music);
            _audioManager.AssignAudioInstance(_backgroundMusic);
            _backgroundMusic.Play();
            _hudManager.ShowAdditional<StartHudMediator>();
            _gameController.ChangeState<GameStartedState>();
        }

        protected override void OnDispose()
        {
            
        }
    }
}