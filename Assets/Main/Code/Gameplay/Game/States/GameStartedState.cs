using Zenject;

namespace Main.Code.Gameplay.Game.States
{
    public class GameStartedState : GameState
    {
        [Inject] private GameView _gameView;
        [Inject] private DiContainer _diContainer;
        protected override void Initialized()
        {
            _gameView.Player.Construct(_diContainer);
        }

        protected override void OnDispose()
        {
            
        }
    }
}