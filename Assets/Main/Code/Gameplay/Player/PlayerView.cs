using System;
using System.Collections;
using Main.Code.Core.State;
using Main.Code.Gameplay.Enemy;
using Main.Code.Gameplay.Player.States;
using UnityEngine;
using Zenject;

namespace Main.Code.Gameplay.Player
{
    public class PlayerView : MonoBehaviour
    {
        [SerializeField] private PlayerConfig _playerConfig;
        [SerializeField] private Animator _playerAnimator;
        [SerializeField] private LayerMask _enemyLayerMask;
        [SerializeField] private Transform _arrowSpawnPos;
        [SerializeField] private Transform _bodyTransform;
        [SerializeField] private GameObject _pointPrefab;
        private StateController _stateController;
        public EnemyView enemyTarget { get; set; }
        public Animator PlayerAnimator => _playerAnimator;
        public event Action OnAnimatorShoot;
        
        public void Construct(DiContainer diContainer)
        {
            _stateController = new StateController();
            diContainer.Inject(_stateController);
           
            _stateController.AddState(new IdleState(this));
            _stateController.AddState(new FindingEnemyState(this, transform, _playerConfig.DetectEnemyRadius, _enemyLayerMask));
            _stateController.AddState(new ShootingState(this, _playerConfig, _arrowSpawnPos, _bodyTransform, _pointPrefab));
            _stateController.ChangeState<IdleState>();
        }
        
        public void ChangeState<T>() where T : PlayerState
        {
            _stateController.ChangeState<T>();
        }

        public void AnimatorShoot()
        {
            OnAnimatorShoot?.Invoke();
        }
    }
}