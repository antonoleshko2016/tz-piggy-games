using System;
using System.Threading.Tasks;
using Main.Code.Gameplay.Arrow;
using Main.Code.Gameplay.Enemy;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Main.Code.Gameplay.Player.States
{
    public class ShootingState : PlayerState
    {
        private EnemyView _enemyView;
        private PlayerConfig _playerConfig;
        private Transform _arrowSpawnPos;
        private static readonly int ShootTrigger = Animator.StringToHash("Aim");
        private bool _isReadyToShoot;
        private Transform _bodyTransform;
        private GameObject _aimingPoints;
        private GameObject[] _points;
        private readonly int _numberOfPoints = 100;
        private readonly float _distanceBetweenPoints = 0.05f;
        private readonly float _shootForce = 25;
        private readonly float _acceleration = 8;
        private Vector2 _direction;
        public event Action OnRecharge;

        public ShootingState(PlayerView playerView, PlayerConfig playerConfig, Transform arrowSpawnPos, Transform bodyTransform, GameObject pointPrefab) : base(playerView)
        {
            _bodyTransform = bodyTransform;
            _arrowSpawnPos = arrowSpawnPos;
            _playerConfig = playerConfig;
            _aimingPoints = Object.Instantiate(new GameObject("Points"), _playerView.transform.position, Quaternion.identity, _playerView.transform);
            _points = new GameObject[_numberOfPoints];
            for (int i = 0; i < _numberOfPoints; i++)
            {
                _points[i] = Object.Instantiate(pointPrefab, _aimingPoints.transform.position, Quaternion.identity,
                    _aimingPoints.transform);
            }
            _aimingPoints.SetActive(false);
            ShootRecharge();
        }
        protected override void Initialized()
        {
            _enemyView = _playerView.enemyTarget;
            _enemyView.OnDeath += TargetDead;
            if (_isReadyToShoot)
            {
                Shoot();
            }
            else
            {
                OnRecharge += Shoot;
            }
        }

        protected override void Update()
        {
            if (_enemyView == null) return;
            if (_enemyView.transform.position.x < _playerView.transform.position.x)
            {
                _playerView.ChangeState<FindingEnemyState>();
                return;
            }
            
        }

        private void Shoot()
        {
            OnRecharge -= Shoot;
            _playerView.PlayerAnimator.SetTrigger(ShootTrigger);
            _playerView.OnAnimatorShoot += Aim;
        }

        private void Aim()
        {
            _playerView.OnAnimatorShoot -= Aim;
            _aimingPoints.SetActive(true);
            for (int i = 1; i < 1000; i++)
            {
                float time = i * 0.01f;
                _direction = CalculateDirectionNormalized(time);
                Vector2 arrowPosition = PointPositionAcceleration(time);
                float diff = (arrowPosition - (Vector2)_enemyView.transform.position).magnitude;
                if (diff is < 0.5f and > -0.5f)
                {
                    break;
                }
            }
     
            float angle = Mathf.Atan2(_direction.y, _direction.x) * Mathf.Rad2Deg + 90;
            Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            _bodyTransform.rotation = rotation;
            
            for (var index = 0; index < _points.Length; index++)
            {
                var point = _points[index];
                point.transform.position = PointPositionAcceleration(index * _distanceBetweenPoints);
            }
            
            SpawnArrow();
        }
        

        private void SpawnArrow()
        {
            _playerView.PlayerAnimator.SetTrigger("Shoot");
            ArrowView arrow = Object.Instantiate(_playerConfig.ArrowView, _arrowSpawnPos.position, _arrowSpawnPos.rotation);
            arrow.Shoot(_bodyTransform.up * -_shootForce, _acceleration);
            OnRecharge += Shoot;
            ShootRecharge();
        }
        protected override void OnDispose()
        {
            _playerView.PlayerAnimator.SetTrigger("CancelShooting");
            _aimingPoints.SetActive(false);
            _bodyTransform.localRotation = Quaternion.identity;
            _playerView.OnAnimatorShoot -= Aim;
            OnRecharge -= Shoot;
            _enemyView.OnDeath -= TargetDead;
            _enemyView = null;
        }

        private void TargetDead()
        {
            _playerView.ChangeState<FindingEnemyState>();
        }
        
        private async void ShootRecharge()
        {
            _isReadyToShoot = false;
            await Task.Delay(TimeSpan.FromSeconds(_playerConfig.ShootRecharge));
            _isReadyToShoot = true;
            OnRecharge?.Invoke();
        }

        private Vector2 PointPosition(float time)
        {
            Vector2 position = (Vector2)_arrowSpawnPos.position + (_direction.normalized * (_shootForce * time)) +
                               Physics2D.gravity * (0.5f * (time * time));
            return position;
        }
        
        private Vector2 PointPositionAcceleration(float time)
        {
            Vector2 directionNormalized = _direction.normalized;
            Vector2 accelerationGravity = Physics2D.gravity * (0.5f * (time * time));
            Vector2 acceleration = directionNormalized * _acceleration * (0.5f * (time * time));
            Vector2 position = (Vector2)_arrowSpawnPos.position + (directionNormalized * _shootForce * time) +
                               accelerationGravity + acceleration;
            return position;
        }
        
        private Vector2 CalculateDirectionNormalized(float time)
        {
            Vector2 startPos = _arrowSpawnPos.position;
            Vector2 endPosition = _enemyView.transform.position;
            Vector2 U = Physics2D.gravity * (0.5f * (time * time)) + startPos - endPosition;
            
            Vector2 directionNormalized =  -U / (_acceleration * (0.5f * (time * time)) * Vector2.one + time * _shootForce *  Vector2.one); 
            
            return directionNormalized.normalized;
        }
    }
}