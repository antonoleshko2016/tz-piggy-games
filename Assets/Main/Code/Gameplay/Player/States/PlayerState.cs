using Main.Code.Core.State;

namespace Main.Code.Gameplay.Player.States
{
    public abstract class PlayerState : State
    {
        protected PlayerView _playerView;

        public PlayerState(PlayerView playerView)
        {
            _playerView = playerView;
        }
    }
}