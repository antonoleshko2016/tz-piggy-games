using Main.Code.Gameplay.Enemy;
using UnityEngine;

namespace Main.Code.Gameplay.Player.States
{
    public class FindingEnemyState : PlayerState
    {
        private Transform _playerTransform;
        private float _detectEnemyRadius;
        private LayerMask _enemyLayerMask;
        private GameObject _shootPoints;
        public FindingEnemyState(PlayerView playerView, Transform playerTransform,  float detectEnemyRadius,  LayerMask enemyLayerMask) : base(playerView)
        {
            _enemyLayerMask = enemyLayerMask;
            _detectEnemyRadius = detectEnemyRadius;
            _playerTransform = playerTransform;
        }
        protected override void Initialized()
        {
  
        }

        protected override void OnDispose()
        {
            
        }

        protected override void Update()
        {
            Collider2D[] colliders = Physics2D.OverlapCircleAll(_playerTransform.position, _detectEnemyRadius, _enemyLayerMask.value);
            
            foreach (var collider in colliders)
            {
                EnemyView enemyView = collider.GetComponent<EnemyView>();
                if (enemyView != null && enemyView.IsAlive && enemyView.transform.position.x > _playerView.transform.position.x)
                {
                    _playerView.enemyTarget = enemyView;
                    _playerView.ChangeState<ShootingState>();
                    return;
                }
            }
        }
    }
}