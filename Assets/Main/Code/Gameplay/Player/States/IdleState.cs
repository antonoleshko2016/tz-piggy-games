namespace Main.Code.Gameplay.Player.States
{
    public class IdleState : PlayerState
    {
        public IdleState(PlayerView playerView) : base(playerView)
        {
        }
        protected override void Initialized()
        {
            _playerView.ChangeState<FindingEnemyState>();
        }

        protected override void OnDispose()
        {
            
        }
    }
}