﻿using Main.Code.Core.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Main.Code.UI
{
    public sealed class StartHudView : BaseHud
    {
        [SerializeField] private Button _spawnEnemyButton;
        public Button SpawnEnemyButton => _spawnEnemyButton;
        protected override void OnEnable()
        {
        }

        protected override void OnDisable()
        {
        }
        
    }
}