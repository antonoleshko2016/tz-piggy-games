﻿using Core.UI;
using Main.Code.Gameplay.Game;
using Zenject;

namespace Main.Code.UI
{
    public sealed class StartHudMediator : Mediator<StartHudView>
    {
        [Inject] private GameController _gameController;
        protected override void Show()
        {
            _view.SpawnEnemyButton.onClick.AddListener(SpawnEnemy);
        }

        protected override void Hide()
        {
            _view.SpawnEnemyButton.onClick.RemoveListener(SpawnEnemy);
        }

        private void SpawnEnemy()
        {
            _gameController.SpawnEnemy();
        }
    }
}