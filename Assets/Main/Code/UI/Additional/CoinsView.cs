using System;
using Main.Code.Core.Additional;
using TMPro;
using UnityEngine;

namespace Main.Code.UI.Additional {
    public class CoinsView : MonoBehaviour, IDisposable
    {
        private Bank _bank;
        private TMP_Text _coinsText;
        private void Initialize(Bank bank)
        {
            _bank = bank;
            _bank.OnCoinsChange += OnCoinsChange;
            OnCoinsChange();
        }
        
        public void Dispose()
        {
            _bank.OnCoinsChange -= OnCoinsChange;
            _bank = null;
        }
        private void OnCoinsChange()
        {
            _coinsText ??= GetComponentInChildren<TMP_Text>();
            _coinsText.text = _bank.Coins.ToString();
        }

       
    }
}
